import Vue from 'vue'
import App from './App'
import router from './router'
import Vuex from 'vuex'

Vue.use(Vuex)

Vue.config.productionTip = false

const store = new Vuex.Store({
  strict: true,
  state: {
    librarian:null,
    currentProject:null,
    selectedTags:[],
  },
  mutations: {
    setLibrary( state, obj ){
      state.librarian = obj      
    },
    setCurrentProject( state, obj ){
      state.currentProject = obj      
    },
    setSelectedTags( state, obj ){
      state.selectedTags = obj      
    }
  }
})


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
