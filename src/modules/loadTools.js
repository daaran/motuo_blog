// loadTools.js
//https://stackoverflow.com/questions/40294870/module-exports-vs-export-default-in-node-js-and-es6
// ========
//module.exports = {

export default  {
   
    fetchAsync: async ( libUrl ) => {
        let response = await fetch( libUrl );
        let data = await response.json();
        return data;
    },
    fetchAsyncMD: async ( libUrl ) => {
        //https://developer.mozilla.org/es/docs/Web/API/Fetch_API/Utilizando_Fetch
        //https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

        // var myHeaders = new Headers();
        // myHeaders.append("Content-Type", "text/plain");
        // myHeaders.append("Content-Length", content.length.toString());
        // myHeaders.append("X-Custom-Header", "ProcessThisImmediately");
  
        // var miInit = { method: 'GET',
        //        headers: myHeaders,
        //        mode: 'no-cors',
        //        cache: 'default' };

        //let response = await fetch( libUrl, miInit );

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "text/plain");
        var miInit = { method: 'GET',
               headers: myHeaders,
               mode: 'cors',
               cache: 'default' };
        let response = await fetch( libUrl, miInit );

        let data = await response;
        return data.text();
      
    },

    // request: async ( libUrl )=>{
    //     var client = new XMLHttpRequest();
    //     client.open('GET', libUrl );
    //     client.onreadystatechange = function() {
    //         return client.responseText;
    //     }
    //     client.send();
    // }
  };
  



  