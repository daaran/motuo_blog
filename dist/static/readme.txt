/assets is for files that are handles by webpack during bundling - for that, they have to be referenced somewhere in your javascript code.

Other assets can be put in /static, the content of this folder will be copied to /dist later as-is. So your above <link> should work if you put bootstrap.min.css inside /static

Tip: make the path absolute if you plan on using vue-router - oitherwise, the changed URLs can make the link point to the wrong path.

<link rel="stylesheet" href="/bootstrap.min.css">
