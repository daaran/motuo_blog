# Motuo Blog

## Repo
https://gitlab.com/daaran/motuo_blog

## Tasks

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

## Deployment

### Webhooks in Gitlab y 1&1
see watch-dog
https://gitlab.com/daaran/motuo_blog/hooks
